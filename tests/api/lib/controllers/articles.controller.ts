import { ApiRequest } from "../request";

let baseUrl: string = "https://knewless.tk/api/";

export class ArticlesController {
  async getArticles(accessToken: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`article/author`)
      .bearerToken(accessToken)
      .send();
    return response;
  }

  async saveArticle(accessToken: string, articleObj: object) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`article`)
      .bearerToken(accessToken)
      .body(articleObj)
      .send();
    return response;
  }

  async saveComment(
    accessToken: string,
    articleIdValue: string,
    idValue: string,
    textValue: string
  ) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`article_comment`)
      .bearerToken(accessToken)
      .body({
        articleId: articleIdValue,
        id: idValue,
        text: textValue,
      })
      .send();
    return response;
  }
}
