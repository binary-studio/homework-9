import { ApiRequest } from "../request";

let baseUrl: string = "https://knewless.tk/api/";

export class FavoritesController {
  async getFavorites(accessToken: string, idVal: string, typeVal: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`favorite?id=${idVal}&type=${typeVal}`)
      .bearerToken(accessToken)
      .send();
    return response;
  }
}
