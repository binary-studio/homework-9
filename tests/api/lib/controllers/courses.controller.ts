import { ApiRequest } from "..request";

export class postLogin {
  async postLogin() {
    const response = await new ApiRequest().prefixUrl("https://knewless.tk");
    .method("POST")
    .url("/api/login")
    .headers(eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJjNjViZDIxOC1mNmRiLTQyMTctYjFjZC0yMmU1MWFjNDE4MzMiLCJpYXQiOjE2NTg3ODkyNzUsImV4cCI6MTY1ODg3NTY3NX0.zXotQRVbmo-0FxO3T0dFincNtNjRiVuh9eeUZ1gUn9RFwhULN-bI2gimc61OdWvbN2OmMhnyFAXIyQ4iBKh0YQ)
    .send();
    return response;
  }

  async getAuthor() {
    const response = await new ApiRequest().prefixUrl("https://knewless.tk");
    .method("GET")
    .url("/api/author/")
    .headers(eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJjNjViZDIxOC1mNmRiLTQyMTctYjFjZC0yMmU1MWFjNDE4MzMiLCJpYXQiOjE2NTg3ODkyNzUsImV4cCI6MTY1ODg3NTY3NX0.zXotQRVbmo-0FxO3T0dFincNtNjRiVuh9eeUZ1gUn9RFwhULN-bI2gimc61OdWvbN2OmMhnyFAXIyQ4iBKh0YQ)
    .send();
    return response;
  }

  async postAuthor() {
    const response = await new ApiRequest().prefixUrl("https://knewless.tk");
    .method("POST")
    .url("/api/author/")
    .headers(eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJjNjViZDIxOC1mNmRiLTQyMTctYjFjZC0yMmU1MWFjNDE4MzMiLCJpYXQiOjE2NTg3ODkyNzUsImV4cCI6MTY1ODg3NTY3NX0.zXotQRVbmo-0FxO3T0dFincNtNjRiVuh9eeUZ1gUn9RFwhULN-bI2gimc61OdWvbN2OmMhnyFAXIyQ4iBKh0YQ)
    .send();
    return response;
  }

  async getUserMe() {
    const response = await new ApiRequest().prefixUrl("https://knewless.tk");
    .method("GET")
    .url("/api/user/me")
    .headers(eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJjNjViZDIxOC1mNmRiLTQyMTctYjFjZC0yMmU1MWFjNDE4MzMiLCJpYXQiOjE2NTg3ODkyNzUsImV4cCI6MTY1ODg3NTY3NX0.zXotQRVbmo-0FxO3T0dFincNtNjRiVuh9eeUZ1gUn9RFwhULN-bI2gimc61OdWvbN2OmMhnyFAXIyQ4iBKh0YQ)
    .send();
    return response;
  }

  async getAuthorOverviewByID(userId: string) {
    const response = await new ApiRequest().prefixUrl("https://knewless.tk");
    .method("GET")
    .url("/api/author/overview/${userId}")
    .headers(eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJjNjViZDIxOC1mNmRiLTQyMTctYjFjZC0yMmU1MWFjNDE4MzMiLCJpYXQiOjE2NTg3ODkyNzUsImV4cCI6MTY1ODg3NTY3NX0.zXotQRVbmo-0FxO3T0dFincNtNjRiVuh9eeUZ1gUn9RFwhULN-bI2gimc61OdWvbN2OmMhnyFAXIyQ4iBKh0YQ)
    .send();
    return response;
  }

  async postArticle() {
    const response = await new ApiRequest().prefixUrl("https://knewless.tk");
    .method("POST")
    .url("/api/article/")
    .headers(eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJjNjViZDIxOC1mNmRiLTQyMTctYjFjZC0yMmU1MWFjNDE4MzMiLCJpYXQiOjE2NTg3ODkyNzUsImV4cCI6MTY1ODg3NTY3NX0.zXotQRVbmo-0FxO3T0dFincNtNjRiVuh9eeUZ1gUn9RFwhULN-bI2gimc61OdWvbN2OmMhnyFAXIyQ4iBKh0YQ)
    .send();
    return response;
  }

  async getArticleAuthor() {
    const response = await new ApiRequest().prefixUrl("https://knewless.tk");
    .method("GET")
    .url("/api/article/author")
    .headers(eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJjNjViZDIxOC1mNmRiLTQyMTctYjFjZC0yMmU1MWFjNDE4MzMiLCJpYXQiOjE2NTg3ODkyNzUsImV4cCI6MTY1ODg3NTY3NX0.zXotQRVbmo-0FxO3T0dFincNtNjRiVuh9eeUZ1gUn9RFwhULN-bI2gimc61OdWvbN2OmMhnyFAXIyQ4iBKh0YQ)
    .send();
    return response;
  }

  async getAtricleById() {
    const response = await new ApiRequest().prefixUrl("https://knewless.tk");
    .method("GET")
    .url("/api/article/{atricleId}")
    .headers(eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJjNjViZDIxOC1mNmRiLTQyMTctYjFjZC0yMmU1MWFjNDE4MzMiLCJpYXQiOjE2NTg3ODkyNzUsImV4cCI6MTY1ODg3NTY3NX0.zXotQRVbmo-0FxO3T0dFincNtNjRiVuh9eeUZ1gUn9RFwhULN-bI2gimc61OdWvbN2OmMhnyFAXIyQ4iBKh0YQ)
    .send();
    return response;
  }

  async postComment() {
    const response = await new ApiRequest().prefixUrl("https://knewless.tk");
    .method("POST")
    .url("/api/article_comment")
    .headers(eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJjNjViZDIxOC1mNmRiLTQyMTctYjFjZC0yMmU1MWFjNDE4MzMiLCJpYXQiOjE2NTg3ODkyNzUsImV4cCI6MTY1ODg3NTY3NX0.zXotQRVbmo-0FxO3T0dFincNtNjRiVuh9eeUZ1gUn9RFwhULN-bI2gimc61OdWvbN2OmMhnyFAXIyQ4iBKh0YQ)
    .send();
    return response;
  }

  async getAtricleByIdSize() {
    const response = await new ApiRequest().prefixUrl("https://knewless.tk");
      .method("GET")
      .url("/api/article_comment/of/{atricleId}?size=200") .headers(eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJjNjViZDIxOC1mNmRiLTQyMTctYjFjZC0yMmU1MWFjNDE4MzMiLCJpYXQiOjE2NTg3ODkyNzUsImV4cCI6MTY1ODg3NTY3NX0.zXotQRVbmo-0FxO3T0dFincNtNjRiVuh9eeUZ1gUn9RFwhULN-bI2gimc61OdWvbN2OmMhnyFAXIyQ4iBKh0YQ)
      .send();
    return response;
  }








  async getCourseComment() {
    const response = await new ApiRequest().prefixUrl("https://knewless.tk");
      .method("POST")
      .url("/api/course_comment")
      .headers(eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIwZDBhNDdkNC1mNmQwLTQ2MjctODA3MS04MmY0ZGRlMGVlNDgiLCJpYXQiOjE2NTg3MzQ1NTksImV4cCI6MTY1ODgyMDk1OX0.qLopDIG6XYJdydbVDZFFrdYYe25CrajJ0YvIX33k7H75tBfGAGTv4ElRxinkD_Haqlx79aWT218yXxavo-_RaQ)
      .send();
    return response;
  }

  async getRecommendation () {
    const response = await new ApiRequest().prefixUrl("https://knewless.tk");
      .method("GET")
      .url("/api/course/recommended")
      .headers(eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIwZDBhNDdkNC1mNmQwLTQ2MjctODA3MS04MmY0ZGRlMGVlNDgiLCJpYXQiOjE2NTg3MzQ1NTksImV4cCI6MTY1ODgyMDk1OX0.qLopDIG6XYJdydbVDZFFrdYYe25CrajJ0YvIX33k7H75tBfGAGTv4ElRxinkD_Haqlx79aWT218yXxavo-_RaQ)
      .send();
    return response;
  }
  async getContinueLearningCourses(id: string) {
    const response = await new ApiRequest().prefixUrl("https://knewless.tk");
      .method("GET")
      .url("/api/course/continue/${id}")
      .headers(eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIwZDBhNDdkNC1mNmQwLTQ2MjctODA3MS04MmY0ZGRlMGVlNDgiLCJpYXQiOjE2NTg3MzQ1NTksImV4cCI6MTY1ODgyMDk1OX0.qLopDIG6XYJdydbVDZFFrdYYe25CrajJ0YvIX33k7H75tBfGAGTv4ElRxinkD_Haqlx79aWT218yXxavo-_RaQ)
      .send();
    return response;
  }
  async getFavouriteArticles() {
    const response = await new ApiRequest().prefixUrl("https://knewless.tk");
      .method("GET")
      .url("/api/favorite/articles")
      .headers(eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIwZDBhNDdkNC1mNmQwLTQ2MjctODA3MS04MmY0ZGRlMGVlNDgiLCJpYXQiOjE2NTg3MzQ1NTksImV4cCI6MTY1ODgyMDk1OX0.qLopDIG6XYJdydbVDZFFrdYYe25CrajJ0YvIX33k7H75tBfGAGTv4ElRxinkD_Haqlx79aWT218yXxavo-_RaQ)
      .send();
    return response;
  }
  async getGoals() {
    const response = await new ApiRequest().prefixUrl("https://knewless.tk");
      .method("GET")
      .url("/api/goals")
      .headers(eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIwZDBhNDdkNC1mNmQwLTQ2MjctODA3MS04MmY0ZGRlMGVlNDgiLCJpYXQiOjE2NTg3MzQ1NTksImV4cCI6MTY1ODgyMDk1OX0.qLopDIG6XYJdydbVDZFFrdYYe25CrajJ0YvIX33k7H75tBfGAGTv4ElRxinkD_Haqlx79aWT218yXxavo-_RaQ)
      .send();
    return response;
  }
  async postSubscribe() {
    const response = await new ApiRequest().prefixUrl("https://knewless.tk");
      .method("GET")
      .url("/api/subscription/subscribe")
      .headers(eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIwZDBhNDdkNC1mNmQwLTQ2MjctODA3MS04MmY0ZGRlMGVlNDgiLCJpYXQiOjE2NTg3MzQ1NTksImV4cCI6MTY1ODgyMDk1OX0.qLopDIG6XYJdydbVDZFFrdYYe25CrajJ0YvIX33k7H75tBfGAGTv4ElRxinkD_Haqlx79aWT218yXxavo-_RaQ)
      .send();
    return response;
  }
}
