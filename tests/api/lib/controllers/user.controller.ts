import { ApiRequest } from "../request";

let baseUrl: string = "https://knewless.tk/api/";

export class UserController {
  async getCurrentUser(accessToken: string) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`user/me`)
      .bearerToken(accessToken)
      .send();
    return response;
  }
}
