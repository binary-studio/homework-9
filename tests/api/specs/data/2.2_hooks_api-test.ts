import { expect } from "chai";
import { checkStatusCode } from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";
import { ArticlesController } from "../lib/controllers/articles.controller";
import { UserController } from "../lib/controllers/user.controller";
const auth = new AuthController();
const articles = new ArticlesController();
const user = new UserController();

xdescribe("Articles controller | with hooks", () => {
  let accessToken: string, userId: string;
  let articlesCounter: number;

  before(`should get access token and userId`, async () => {
    // runs once before the first test in this block
    let response = await auth.authenticateUser(
      "zoriana.chyzhovska@gmail.com",
      "ZTest 2021"
    );
    checkStatusCode(response, 200);
    accessToken = response.body.accessToken;

    response = await user.getCurrentUser(accessToken);
    checkStatusCode(response, 200);
    userId = response.body.id;
  });

  it(`get articles`, async () => {
    let response = await articles.getArticles(accessToken);
    checkStatusCode(response, 200);

    articlesCounter = response.body.length;
  });

  it(`add article`, async () => {
    let newArticle = {
      authorId: userId,
      authorName: "ZorianaQA",
      name: "My second test article",
      text: "Hey, this is my second test article. Enjoy!",
    };

    let response = await articles.saveArticle(accessToken, newArticle);
    checkStatusCode(response, 200);

    articlesCounter += 1;
  });

  it(`get articles`, async () => {
    let response = await articles.getArticles(accessToken);
    checkStatusCode(response, 200);

    expect(response.body.length).to.be.equal(articlesCounter);
  });

  afterEach(function () {
    // runs after each test in this block
    console.log("It was a test");
  });
});
