import { expect } from "chai";
import { CoursesController } from "../lib/controllers/courses.controller";
const courses = new CoursesController();
const schemas = require("./data/schemas_testData.json");
var chai = require("chai");
chai.use(require("chai-json-schema"));

describe("Courses controller", () => {
  it("postLogin"),
    async () => {
      let response = await courses.postLogin();

      // console.log(response.body);

      expect(response.statusCode, "Status code should be 200").to.be.equal(200);
      expect(
        response.timings.phases.total,
        "Response time should be less than 5s"
      ).to.be.lessThan(5000);
    };

  it("getAuthor"),
    async () => {
      let response = await courses.getAuthor();

      // console.log(response.body);

      expect(response.statusCode, "Status code should be 200").to.be.equal(200);
      expect(
        response.timings.phases.total,
        "Response time should be less than 5s"
      ).to.be.lessThan(5000);

      expect(response.body).to.be.jsonSchema(schemas.getAuthor);
    };

  it("postAuthor"),
    async () => {
      let response = await courses.postAuthor();

      // console.log(response.body);

      expect(response.statusCode, "Status code should be 200").to.be.equal(200);
      expect(
        response.timings.phases.total,
        "Response time should be less than 5s"
      ).to.be.lessThan(5000);

      expect(response.body).to.be.jsonSchema(schemas.postAuthor);
    };

  it("getUserMe"),
    async () => {
      let response = await courses.getUserMe();

      // console.log(response.body);

      expect(response.statusCode, "Status code should be 200").to.be.equal(200);
      expect(
        response.timings.phases.total,
        "Response time should be less than 5s"
      ).to.be.lessThan(5000);

      expect(response.body).to.be.jsonSchema(schemas.getUserMe);
    };

  it("getAuthorOverviewByID"),
    async () => {
      let response = await courses.getAuthorOverviewByID();

      // console.log(response.body);

      expect(response.statusCode, "Status code should be 200").to.be.equal(200);
      expect(
        response.timings.phases.total,
        "Response time should be less than 5s"
      ).to.be.lessThan(5000);
    };

  it("postArticle"),
    async () => {
      let response = await courses.postArticle();

      // console.log(response.body);

      expect(response.statusCode, "Status code should be 200").to.be.equal(200);
      expect(
        response.timings.phases.total,
        "Response time should be less than 5s"
      ).to.be.lessThan(5000);

      expect(response.body).to.be.jsonSchema(schemas.postArticle);
    };

  it("getArticleAuthor"),
    async () => {
      let response = await courses.getArticleAuthor();

      // console.log(response.body);

      expect(response.statusCode, "Status code should be 200").to.be.equal(200);
      expect(
        response.timings.phases.total,
        "Response time should be less than 5s"
      ).to.be.lessThan(5000);

      expect(response.body).to.be.jsonSchema(schemas.getArticleAuthor);
    };

  it("getAtricleById"),
    async () => {
      let response = await courses.getAtricleById();

      // console.log(response.body);

      expect(response.statusCode, "Status code should be 200").to.be.equal(200);
      expect(
        response.timings.phases.total,
        "Response time should be less than 5s"
      ).to.be.lessThan(5000);

      expect(response.body).to.be.jsonSchema(schemas.getAtricleById);
    };

  it("postComment"),
    async () => {
      let response = await courses.postComment();

      // console.log(response.body);

      expect(response.statusCode, "Status code should be 200").to.be.equal(200);
      expect(
        response.timings.phases.total,
        "Response time should be less than 5s"
      ).to.be.lessThan(5000);

      expect(response.body).to.be.jsonSchema(schemas.postComment);
    };

  it("getAtricleByIdSize"),
    async () => {
      let response = await courses.getAtricleByIdSize();

      // console.log(response.body);

      expect(response.statusCode, "Status code should be 200").to.be.equal(200);
      expect(
        response.timings.phases.total,
        "Response time should be less than 5s"
      ).to.be.lessThan(5000);

      expect(response.body).to.be.jsonSchema(schemas.getAtricleByIdSize);
    };

  it("getCourseComment"),
    async () => {
      let response = await courses.getCourseCommente();

      // console.log(response.body);

      expect(response.statusCode, "Status code should be 200").to.be.equal(200);
      expect(
        response.timings.phases.total,
        "Response time should be less than 5s"
      ).to.be.lessThan(5000);

      expect(response.body).to.be.jsonSchema(schemas.schema_courseComment);
    };

  it("getRecommendation"),
    async () => {
      let response = await courses.getRecommendation();

      // console.log(response.body);

      expect(response.statusCode, "Status code should be 200").to.be.equal(200);
      expect(
        response.timings.phases.total,
        "Response time should be less than 5s"
      ).to.be.lessThan(5000);

      expect(response.body).to.be.jsonSchema(schemas.getRecommendation);
    };

  it("getContinueLearningCourses"),
    async () => {
      let response = await courses.getContinueLearningCourses();

      // console.log(response.body);

      expect(response.statusCode, "Status code should be 200").to.be.equal(200);
      expect(
        response.timings.phases.total,
        "Response time should be less than 5s"
      ).to.be.lessThan(5000);

      expect(response.body).to.be.jsonSchema(
        schemas.getContinueLearningCourses
      );
    };

  it("getFavouriteArticles"),
    async () => {
      let response = await courses.getFavouriteArticles();

      // console.log(response.body);

      expect(response.statusCode, "Status code should be 200").to.be.equal(200);
      expect(
        response.timings.phases.total,
        "Response time should be less than 5s"
      ).to.be.lessThan(5000);

      expect(response.body).to.be.jsonSchema(schemas.getFavouriteArticles);
    };

  it("getGoals"),
    async () => {
      let response = await courses.getGoals();

      // console.log(response.body);

      expect(response.statusCode, "Status code should be 200").to.be.equal(200);
      expect(
        response.timings.phases.total,
        "Response time should be less than 5s"
      ).to.be.lessThan(5000);

      expect(response.body).to.be.jsonSchema(schemas.getGoals);
    };

  it("postSubscribe"),
    async () => {
      let response = await courses.postSubscribe();

      // console.log(response.body);

      expect(response.statusCode, "Status code should be 200").to.be.equal(200);
      expect(
        response.timings.phases.total,
        "Response time should be less than 5s"
      ).to.be.lessThan(5000);
    };

  it("postSubscribe"),
    async () => {
      let response = await courses.postSubscribe();

      // console.log(response.body);

      expect(response.statusCode, "Status code should be 201").to.be.equal(201);
    };

  it("getGoals"),
    async () => {
      let response = await courses.getGoals();

      // console.log(response.body);

      expect(
        response.timings.phases.total,
        "Response time should be less than 5s"
      ).to.be.moreThan(5000);
    };

  it("getFavouriteArticles"),
    async () => {
      let response = await courses.getFavouriteArticles();

      // console.log(response.body);

      expect(response.statusCode, "Status code should be 201").to.be.equal(201);
      expect(
        response.timings.phases.total,
        "Response time should be less than 5s"
      ).to.be.moreThan(5000);
    };
});
