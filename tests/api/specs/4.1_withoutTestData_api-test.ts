import {
  checkResponseTime,
  checkStatusCode,
  checkResponseBodyStatus,
  checkResponseBodyMessage,
} from "../../helpers/functionsForChecking.helper";
import { AuthController } from "../lib/controllers/auth.controller";
const auth = new AuthController();

xdescribe("Without test data", () => {
  it(`login using invalid credentials email: 'zoriana.qa.test@gmail.com', password: '      '`, async () => {
    let response = await auth.authenticateUser(
      "zoriana.qa.test@gmail.com",
      "      "
    );

    checkStatusCode(response, 401);
    checkResponseBodyStatus(response, "UNAUTHORIZED");
    checkResponseBodyMessage(response, "Bad credentials");
    checkResponseTime(response, 3000);
  });

  it(`login using invalid credentials email: 'zoriana.qa.test@gmail.com', password: '  ZTest2021'`, async () => {
    let response = await auth.authenticateUser(
      "zoriana.qa.test@gmail.com",
      "  ZTest2021"
    );

    checkStatusCode(response, 401);
    checkResponseBodyStatus(response, "UNAUTHORIZED");
    checkResponseBodyMessage(response, "Bad credentials");
    checkResponseTime(response, 3000);
  });

  it(`login using invalid credentials email: 'zoriana.qa.test@gmail.com', password: 'ZTest 2021'`, async () => {
    let response = await auth.authenticateUser(
      "zoriana.qa.test@gmail.com",
      "ZTest 2021"
    );

    checkStatusCode(response, 401);
    checkResponseBodyStatus(response, "UNAUTHORIZED");
    checkResponseBodyMessage(response, "Bad credentials");
    checkResponseTime(response, 3000);
  });

  it(`login using invalid credentials email: 'zoriana.qa.test@gmail.com', password: 'zoriana.qa.test@gmail.com'`, async () => {
    let response = await auth.authenticateUser(
      "zoriana.qa.test@gmail.com",
      "zoriana.qa.test@gmail.com"
    );

    checkStatusCode(response, 401);
    checkResponseBodyStatus(response, "UNAUTHORIZED");
    checkResponseBodyMessage(response, "Bad credentials");
    checkResponseTime(response, 3000);
  });

  it(`login using invalid credentials email: 'zoriana.qa.test @gmail.com', password: 'ZTest2021'`, async () => {
    let response = await auth.authenticateUser(
      "zoriana.qa.test @gmail.com",
      "ZTest2021"
    );

    checkStatusCode(response, 401);
    checkResponseBodyStatus(response, "UNAUTHORIZED");
    checkResponseBodyMessage(response, "Bad credentials");
    checkResponseTime(response, 3000);
  });

  it(`login using invalid credentials email: 'zoriana.qa.test @gmail.com', password: 'admin'`, async () => {
    let response = await auth.authenticateUser(
      "zoriana.qa.test @gmail.com",
      "admin"
    );

    checkStatusCode(response, 401);
    checkResponseBodyStatus(response, "UNAUTHORIZED");
    checkResponseBodyMessage(response, "Bad credentials");
    checkResponseTime(response, 3000);
  });

  it(`login using invalid credentials email: 'zoriana.qa.test @gmail.com ', password: 'ZTest2021'`, async () => {
    let response = await auth.authenticateUser(
      "zoriana.qa.test@gmail.com ",
      "ZTest2021"
    );

    checkStatusCode(response, 401);
    checkResponseBodyStatus(response, "UNAUTHORIZED");
    checkResponseBodyMessage(response, "Bad credentials");
    checkResponseTime(response, 3000);
  });
});
