Repository for API test project

API Testing Framework
Test runner - Mocha
Request lib - got
Assertions - Chai

Setup

Install NodeJS - LTS version is preferable
Clone the repository
Locate to the project folder and install dependencies. - npm install

Run the script command from the package.json

Structure of the project

├───tests
├───api
│ ├───config
│ ├───lib
│ │ ├───controllers
│ │ │ | ├───<functionality-name>.controller.ts
│ │ │ | ├───...
│ │ │ ├───request.ts
│ │───specs
│ │ ├───<functionality-name>
│ │ | ├───data
│ │ | ├───...api-test.ts
│ │ ├───...
├───helpers
├───...

scripts - bash scripts, js modules for particular job to be done.
test/api - test root folder with project folders.
test/helpers - helpers to use across the tests
api/config - directory with configs to run tests depends on env
api/lib - all controllers, request builder and other important files are in here
api/specs - put your tests here inside corresponding folder
